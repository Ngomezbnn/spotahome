CREATE DATABASE
  IF NOT EXISTS spotahome
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_general_ci;

USE spotahome;

CREATE TABLE properties (
  id INT NOT NULL,
  title TEXT,
  link TEXT,
  city VARCHAR(100),
  image_url TEXT,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
