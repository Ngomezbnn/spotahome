.ONESHELL:
SHELL := /bin/bash

include docker/docker.env
export $(shell sed 's/=.*//' docker/docker.env)

compose_path := docker/docker-compose.yml

bash-backend: .bash-backend ## Open ashell for the backend container

bash-webserver: .bash-webserver ## Open ashell for the webserver container

build-images: ## Build all the images
	@docker-compose -f $(compose_path) build

build-images-nocache: ## Build all the images
	@docker-compose -f $(compose_path) build --no-cache

help: ## Displays this help
	@echo 'Usage: make [target] ...'
	@echo
	@echo 'targets:'
	@echo -e "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"

init:
	make build-images
	make install-dependencies
	make run

install-dependencies: ## Run composer-install in backend container
	docker-compose -f $(compose_path) exec -T backend sh -c "composer install"

kill: ## Kills all the containers
	@docker-compose -f $(compose_path) down

logs-backend: .logs-backend ## Shows the logs for backend service
logs-database: .logs-database ## Shows the logs for database service
logs-webserver: .logs-webserver ## Shows the logs for webserver service

restart: kill run ## Apply "kill" and "run" makefile targets

run: ## Start backend and webserver containers
	@docker-compose -f $(compose_path) up -d

status: ## Shows the status of the services
	@docker-compose -f $(compose_path) ps

test:
	docker-compose -f $(compose_path) exec -T backend sh -c "vendor/bin/phpunit"

## Makefile patterns
.bash-%:
	@docker-compose -f $(compose_path) exec $* bash

.logs-%:
	@docker-compose -f $(compose_path) logs $*

## Default command to be executed if nothing provided
.DEFAULT_GOAL := help
