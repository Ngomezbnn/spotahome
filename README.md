#Spotahome challenge

## How to deploy

1.   `git clone https://gitlab.com/Ngomezbnn/spotahome.git`
2.   `cp docker/docker.env.example docker/docker.env`
3.   `make init`

## How to import data

The import process can be launched as many times as you want, if any property already exists it will just update
the values.

1.  `make bash-backend`
2.  `php bin/console spotahome:import`

[![asciicast](https://asciinema.org/a/235942.svg)](https://asciinema.org/a/235942)

## Testing

I have created tests for two classes because of the amount of time. In order to run the tests execute `make test`

[![asciicast](https://asciinema.org/a/235952.svg)](https://asciinema.org/a/235952)

## Missing

 - Frontend
 - A method to retrieve data using limit and offset for frontend, now only getAll implemented
 - Download a json file with the data sorted as in screen
 (This will be done the same way as the API call, keeping the table sorting state and process the response in a file)
 - Finishing gitlab-ci configuration at gitlab panel