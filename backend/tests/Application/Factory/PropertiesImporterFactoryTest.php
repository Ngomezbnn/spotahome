<?php

namespace App\Tests\Application\Factory;


use App\Application\Factory\PropertiesImporterFactory;
use App\Application\Service\Importer\PropertiesFeedImporter;
use App\Infrastructure\Properties\Repository\MysqlPropertyRepository;
use PHPUnit\Framework\TestCase;

class PropertiesImporterFactoryTest extends TestCase
{
    public function testCreatePropertiesFeedImporter()
    {
        $repositoryMock = $this->getMockBuilder(MysqlPropertyRepository::class)
                               ->disableOriginalConstructor()
                               ->getMock();
        $factory = new PropertiesImporterFactory($repositoryMock);

        $propertiesImporter = $factory->createPropertiesFeedImporter();

        $this->assertInstanceOf(PropertiesFeedImporter::class, $propertiesImporter);
    }
}