<?php

namespace App\Tests\Application\Factory;

use App\Application\Service\Importer\PropertiesFeedImporter;
use App\Infrastructure\Properties\Repository\MysqlPropertyRepository;
use App\Infrastructure\Properties\Repository\PropertyRepositoryInterface;
use PHPUnit\Framework\TestCase;

class PropertiesFeedImporterTest extends TestCase
{
    /**
     * @var PropertyRepositoryInterface
     */
    private $propertyRepository;

    /**
     * @var PropertiesFeedImporter
     */
    private $service;

    protected function setUp(): void
    {
        $this->propertyRepository = $this->getMockBuilder(MysqlPropertyRepository::class)
                                         ->disableOriginalConstructor()
                                         ->getMock();
        $this->service            = new PropertiesFeedImporter($this->propertyRepository);
    }

    public function testLoadIsUsingGenerators()
    {
        $feedImportMock = $this->getMockBuilder(PropertiesFeedImporter::class)
                               ->disableOriginalConstructor()
                               ->setMethods(['retrieveProperties'])
                               ->getMock();

        $feedImportMock->method('retrieveProperties')
                       ->willReturn($this->getPropertyFixture());

        $property = $this->service->import("http://foo.es/xml");

        $this->assertInstanceOf(\Generator::class, $property);
    }

    private function getPropertyFixture()
    {
        return new \SimpleXMLElement($this->getXMLasString());
    }

    private function getXMLasString(): string
    {
        return '<?xml version="1.0" encoding="utf-8"?>
<trovit>
    <ad>            
        <id><![CDATA[95504]]></id>
            <url><![CDATA[https://www.spotahome.com/dublin/for-rent:apartments/95504?utm_source=trovit&utm_medium=cffeeds&utm_campaign=normalads]]></url>
        <title><![CDATA[Spacious, fully furnished rooms for rent in a house with a garden in East Wall]]></title>
            <type><![CDATA[roommate]]></type>
            <content><![CDATA[Foo]]></content>
        <price currency="EUR"><![CDATA[350]]></price>
        <property_type><![CDATA[apartment]]></property_type>
        <foreclosure><![CDATA[0]]></foreclosure>
        <address><![CDATA[Fairfield Avenue]]></address>
        <city><![CDATA[Dublin]]></city>
            <latitude><![CDATA[53.35516464]]></latitude>
            <longitude><![CDATA[-6.23502499]]></longitude>
            <agency><![CDATA[spotahome]]></agency>
            <rooms><![CDATA[4]]></rooms>
            <pictures>
                <picture>
                    <picture_url><![CDATA[http://foo.es/image.jpg]]></picture_url>
                    <picture_title><![CDATA[Front Garden]]></picture_title>
                </picture>
            </pictures>
        <date><![CDATA[ 25/07/2018]]></date>
        <by_owner><![CDATA[0]]></by_owner>
        <parking><![CDATA[0]]></parking>
        <is_furnished><![CDATA[1]]></is_furnished>
        <pool_access><![CDATA[0]]></pool_access>
        <dishwasher><![CDATA[0]]></dishwasher>
        <terrace_balcony><![CDATA[0]]></terrace_balcony>
        <washing_machine><![CDATA[1]]></washing_machine>
        <air_conditioning><![CDATA[0]]></air_conditioning>
        <smoking_allowed><![CDATA[0]]></smoking_allowed>
        <equipped_kitchen><![CDATA[1]]></equipped_kitchen>
        <oven><![CDATA[1]]></oven>
        <heating_system><![CDATA[1]]></heating_system>
        <dryer><![CDATA[0]]></dryer>
        <number_rooms><![CDATA[4]]></number_rooms>
        <number_bathrooms><![CDATA[2]]></number_bathrooms>
    </ad>
</trovit>';
    }
}