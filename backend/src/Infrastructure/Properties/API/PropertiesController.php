<?php

namespace App\Infrastructure\Properties\API;

use App\Infrastructure\Properties\Repository\MysqlPropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class PropertiesController extends AbstractController
{
    /**
     * @Route("/properties", methods={"GET"})
     */
    public function showProperties(Request $request, MysqlPropertyRepository $repository)
    {
        try {
            $sorting = $request->query->get('sorting') ?? null;
            $properties = $repository->getAll($sorting);

            return new JsonResponse(["data" => $properties]);
        } catch (\Throwable $t) {
            return new JsonResponse(["error" => $t->getMessage()], 500);
        }
    }

}