<?php declare(strict_types=1);

namespace App\Infrastructure\Properties\Repository;

use App\Domain\Model\Property;
use Doctrine\DBAL\Connection;

/**
 * Class MysqlPropertyRepository
 *
 * @package App\Infrastructure\Repository
 */
class MysqlPropertyRepository implements PropertyRepositoryInterface
{
    /** @var Connection */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @inheritdoc
     */
    public function save(Property $property): void
    {
        $query = '
            INSERT INTO properties 
              (id, title, link, city, image_url)
            VALUES 
              (:id, :title, :link, :city, :image_url)
            ON DUPLICATE KEY UPDATE
              title = :title,
              link = :link,
              city = :city,
              image_url = :image_url
        ';

        try {
            $this->connection->executeQuery($query, [
                'id' => $property->getId(),
                'title' => $property->getTitle(),
                'link' => $property->getLink(),
                'city' => $property->getCity(),
                'image_url' => $property->getImageUrl(),
            ]);
        } catch (\Throwable $t) {
            throw new \RuntimeException($t->getMessage());
        }
    }

    /**
     * @param string|null $sorting
     *
     * @return array
     */
    public function getAll(string $sorting = null): array
    {
        $query = '
            SELECT * 
            FROM properties;
        ';

        /**
         * I actually don't like this piece of code,
         * this could be super great to be refactored using the builder pattern
         */
        if (!empty($sorting) && in_array($sorting, Property::getSortableFields())) {
            $query = "
                SELECT * 
                FROM properties
                ORDER BY $sorting
            ";
        }

        try {
            $statement = $this->connection->executeQuery($query);

            return $statement->fetchAll();
        } catch (\Throwable $t) {
            throw new \RuntimeException($t->getMessage());
        }
    }
}
