<?php declare(strict_types=1);

namespace App\Infrastructure\Properties\Repository;

use App\Domain\Model\Property;

/**
 * Interface PropertyRepositoryInterface
 *
 * @package App\Repository
 */
interface PropertyRepositoryInterface
{
    /**
     * @param Property $property
     */
    public function save(Property $property): void;

    /**
     * @param string $sorting
     *
     * @return array
     */
    public function getAll(string $sorting = null): array;
}
