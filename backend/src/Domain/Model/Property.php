<?php declare(strict_types=1);

namespace App\Domain\Model;

/**
 * Class Property
 *
 * @package App\Domain\Model
 */
class Property
{
    /** @var int */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $link;

    /** @var string */
    private $city;

    /** @var string */
    private $imageUrl;

    /**
     * Property constructor.
     *
     * @param int $id
     * @param string $title
     * @param string $link
     * @param string $city
     * @param string $imageUrl
     */
    public function __construct(int $id, string $title, string $link, string $city, string $imageUrl)
    {
        $this->id       = $id;
        $this->title    = $title;
        $this->link     = $link;
        $this->city     = $city;
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return array
     */
    public static function getSortableFields(): array
    {
        return [
            'id',
            'title',
            'link',
            'city',
            'imageUrl',
        ];
    }
}
