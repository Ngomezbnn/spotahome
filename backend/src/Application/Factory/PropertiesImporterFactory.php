<?php declare(strict_types=1);

namespace App\Application\Factory;

use App\Application\Service\Importer\PropertiesFeedImporter;
use App\Infrastructure\Properties\Repository\PropertyRepositoryInterface;

/**
 * Class PropertiesImporterFactory
 *
 * @package App\Application\Factory
 */
class PropertiesImporterFactory
{
    /** @var PropertyRepositoryInterface */
    private $propertyRepository;

    /**
     * PropertiesImporterFactory constructor.
     *
     * @param PropertyRepositoryInterface $propertyRepository
     */
    public function __construct(PropertyRepositoryInterface $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * Creates a Feed importer object
     *
     * @return PropertiesFeedImporter
     */
    public function createPropertiesFeedImporter(): PropertiesFeedImporter
    {
        return new PropertiesFeedImporter($this->propertyRepository);
    }
}
