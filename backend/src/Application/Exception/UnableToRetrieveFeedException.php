<?php

namespace App\Application\Exception;


class UnableToRetrieveFeedException extends \RuntimeException
{
}