<?php declare(strict_types=1);

namespace App\Application\Service\Importer;

use App\Domain\Model\Property;

/**
 * Interface PropertiesImporterInterface
 *
 * @package App\Application\Service\Importer
 */
interface PropertiesImporterInterface
{
    /**
     * Load a properties feed from the given URL.
     *
     * @param string $url
     *
     * @return \Generator
     *
     * @throws \RuntimeException if the given URL cannot be loaded
     */
    public function import(string $url): \Generator;

    /**
     * Persist a property.
     *
     * @param Property $property
     */
    public function save(Property $property): void;
}
