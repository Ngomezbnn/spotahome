<?php declare(strict_types=1);

namespace App\Application\Service\Importer;

use App\Application\Exception\UnableToRetrieveFeedException;
use App\Domain\Model\Property;
use App\Infrastructure\Properties\Repository\PropertyRepositoryInterface;

/**
 * Class PropertiesFeedImporter
 *
 * @package App\Application\Service\Importer
 */
class PropertiesFeedImporter implements PropertiesImporterInterface
{
    /**
     * @var PropertyRepositoryInterface
     */
    private $propertyRepository;

    /**
     * FeedPropertiesImporter constructor.
     *
     * @param PropertyRepositoryInterface $propertyRepository
     */
    public function __construct(PropertyRepositoryInterface $propertyRepository)
    {
        $this->propertyRepository = $propertyRepository;
    }

    /**
     * @param string $url
     *
     * @return \Generator
     */
    public function import(string $url): \Generator
    {
        foreach ($this->retrieveProperties($url) as $property) {
            yield new Property(
                intval($property->id),
                strval($property->title),
                strval($property->link),
                strval($property->city),
                strval($property->imagenUrl)
            );
        }
    }

    /**
     * @param Property $property
     */
    public function save(Property $property): void
    {
        $this->propertyRepository->save($property);
    }

    /**
     * @param string $url
     *
     * @return \Generator
     *
     * @throws \RuntimeException if the given URL cannot be loaded
     */
    protected function retrieveProperties(string $url): \Generator
    {
        try {
            foreach ($this->retrieveFeedDataByUrl($url) as $item) {
                yield $item;
            }
        } catch (\Throwable $t) {
            throw new UnableToRetrieveFeedException($t->getMessage());
        }
    }

    /**
     * @param string $url
     * @return \SimpleXMLElement
     */
    protected function retrieveFeedDataByUrl(string $url): \SimpleXMLElement
    {
        return new \SimpleXMLElement($url, 0, true);
    }
}
