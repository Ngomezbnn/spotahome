<?php declare(strict_types=1);

namespace App\Application\Command;

use App\Application\Service\Importer\PropertiesImporterInterface;
use App\Domain\Model\Property;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PropertyImporterCommand
 *
 * @package App\Application\Command
 */
class PropertyImporterCommand extends Command
{
    /** @var string */
    protected static $defaultName = 'spotahome:import';

    /** @var string */
    private static $ImportSource = "http://feeds.spotahome.com/trovit-Ireland.xml";

    /**
     * @var PropertiesImporterInterface
     */
    private $propertyImporter;

    /**
     * PropertyImporterCommand constructor
     *
     * @param PropertiesImporterInterface $propertyImporter
     */
    public function __construct(PropertiesImporterInterface $propertyImporter)
    {
        parent::__construct();

        $this->propertyImporter = $propertyImporter;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Import process has been started...');

        /** @var Property $property */
        foreach ($this->propertyImporter->import(self::$ImportSource) as $property) {
            $output->writeln(
                sprintf("New property %s imported with id: %s", $property->getTitle(), $property->getId())
            );
            $this->propertyImporter->save($property);
        }

        $output->writeln('Import process has been finished!');
    }
}
